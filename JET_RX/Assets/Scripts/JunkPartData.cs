﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JetRx
{
    public class JunkPartData
    {
        public enum ColorType
        {
            Red, Blue, Yellow
        }

        public ColorType Color { get; set; }

        public enum MainShapeType
        {
            Head, Body, Arm, Leg
        }

        public MainShapeType MainShape { get; set; }


        public enum SubShapeType
        {
            LongAndSquare, LongAndRoundness, ShortAndSquare, ShortAndRoundness
        }

        public SubShapeType SubShape { get; set; }
    }
}