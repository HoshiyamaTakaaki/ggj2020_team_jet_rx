﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JetRx
{
    public class UICustomer : MonoBehaviour, IDropHandler
    {
        /// <summary>
        /// 必要としているパーツの情報
        /// </summary>
        public List<JunkPartData> NeedPartDatas { get; set; } = new List<JunkPartData>();


        public void OnDrop(PointerEventData eventData)
        {
            var part = eventData.pointerDrag.GetComponent<UIJunkPart>();
            if (!part)
            {
                return;
            }

            var data = part.Data;

            // ドラッグされたパーツに応じて処理を分岐
            for (int i = 0; i < NeedPartDatas.Count; ++i)
            {
                if (NeedPartDatas[i].Color == data.Color && NeedPartDatas[i].MainShape == data.MainShape)
                {
                    // 完全に一致したときの処理を書く
                    return;
                }
                else if (NeedPartDatas[i].Color == data.Color || NeedPartDatas[i].MainShape == data.MainShape)
                {
                    // 片方だけ一致しているときの処理を書く
                    return;
                }
            }

            // 全く欲しくないパーツだった時の処理を書く
        }
    }
}