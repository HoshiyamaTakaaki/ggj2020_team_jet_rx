﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace JetRx
{
    public class UIJunkPart : MonoBehaviour
    {
        private RectTransform cachedRectTransform;
        private Image image;

        [SerializeField] private TextMeshProUGUI colorText;
        [SerializeField] private TextMeshProUGUI mainShapeText;
        [SerializeField] private TextMeshProUGUI subShapeText;
        [SerializeField] private Image SelectedImage;

        public System.Action<UIJunkPart> onClickAction;

        private bool isSelected = false;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                SelectedImage.gameObject.SetActive(value);
                onClickAction(this);
            }
        }

        public int SlotNum = 0;

        private JunkPartData data;
        public JunkPartData Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                colorText.text = data.Color.ToString();
                mainShapeText.text = data.MainShape.ToString();
                subShapeText.text = data.SubShape.ToString();
            }
        }

        private void Start()
        {
            cachedRectTransform = GetComponent<RectTransform>();
            image = GetComponent<Image>();
        }

        public void OnClick()
        {
            IsSelected = !isSelected;
        }
    }
}