﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JetRx
{
    public class UIMain : MonoBehaviour
    {
        #region キャラ表示

        [SerializeField] private RectTransform[] charaSlot = new RectTransform[4];

        public void ShowCharaImage(Image charaImage, int slotNum)
        {

        }

        #endregion

        #region 満足度

        [SerializeField] private Slider satisfactionSlider;

        private const int SatisfactionPointMax = 100;
        private const int SatisfactionPointDefault = 50;

        private int satisfactionPoint = 0;
        public int SatisfactionPoint
        {
            get
            {
                return satisfactionPoint;
            }
            set
            {
                if (value > SatisfactionPointMax)
                {
                    value = SatisfactionPointMax;
                }
                else if (value < 0)
                {
                    value = 0;
                }

                satisfactionPoint = value;
                satisfactionSlider.value = satisfactionPoint;
            }
        }

        #endregion

        #region ジャンクパーツ

        [SerializeField] private RectTransform junkPartZone;
        [SerializeField] private RectTransform[] junkPartSlots;

        private Dictionary<int, UIJunkPart> junkPartsDic = new Dictionary<int, UIJunkPart>();

        [SerializeField] private UIJunkPart junkPart;

        /// <summary>
        /// 補充ボタン
        /// </summary>
        public void OnClickReplace()
        {
            if (junkPartsDic.Count >= 6)
            {
                Debug.Log(junkPartsDic.Count);

                foreach (var v in junkPartsDic)
                {
                    Debug.Log(v.Key);
                }

                return;
            }

            var data = new JunkPartData();

            data.Color = (JunkPartData.ColorType)Random.Range(0, System.Enum.GetNames(typeof(JunkPartData.ColorType)).Length);
            data.MainShape = (JunkPartData.MainShapeType)Random.Range(0, System.Enum.GetNames(typeof(JunkPartData.MainShapeType)).Length);
            data.SubShape = (JunkPartData.SubShapeType)Random.Range(0, System.Enum.GetNames(typeof(JunkPartData.SubShapeType)).Length);

            var obj = Instantiate(junkPart.gameObject, junkPartZone);
            var newJunkPart = obj.GetComponent<UIJunkPart>();
            newJunkPart.Data = data;

            AddJunkPart(newJunkPart);
        }

        public void AddJunkPart(UIJunkPart part)
        {
            for (int i = 0; i < 6; ++i)
            {
                if (!junkPartsDic.ContainsKey(i))
                {
                    junkPartsDic.Add(i, part);
                    part.transform.position = junkPartSlots[i].transform.position;
                    part.SlotNum = i;
                    part.onClickAction = OnClickPart;
                    return;
                }
            }
        }

        public void RemoveJunkPart(int slot)
        {
            junkPartsDic.Remove(slot);
        }

        #endregion

        #region リペア

        public void OnClickRepair()
        {
            if (repairList.Count == 0)
            {
                return;
            }

            Dictionary<JunkPartData.ColorType, int> maxColor = new Dictionary<JunkPartData.ColorType, int>();
            Dictionary<JunkPartData.MainShapeType, int> maxMainShape = new Dictionary<JunkPartData.MainShapeType, int>();
            Dictionary<JunkPartData.SubShapeType, int> maxSubShape = new Dictionary<JunkPartData.SubShapeType, int>();

            foreach (var v in junkPartsDic)
            {
                if (repairList.Contains(v.Key))
                {
                    if (maxColor.ContainsKey(v.Value.Data.Color))
                    {
                        maxColor[v.Value.Data.Color]++;
                    }
                    else
                    {
                        maxColor.Add(v.Value.Data.Color, 1);
                    }

                    if (maxMainShape.ContainsKey(v.Value.Data.MainShape))
                    {
                        maxMainShape[v.Value.Data.MainShape]++;
                    }
                    else
                    {
                        maxMainShape.Add(v.Value.Data.MainShape, 1);
                    }

                    if (maxSubShape.ContainsKey(v.Value.Data.SubShape))
                    {
                        maxSubShape[v.Value.Data.SubShape]++;
                    }
                    else
                    {
                        maxSubShape.Add(v.Value.Data.SubShape, 1);
                    }
                }
            }



        }

        #endregion

        private List<int> repairList = new List<int>();

        private void OnClickPart(UIJunkPart part)
        {
            if (part.IsSelected)
            {
                if (!repairList.Contains(part.SlotNum))
                {
                    repairList.Add(part.SlotNum);
                }
            }
            else
            {
                if (repairList.Contains(part.SlotNum))
                {
                    repairList.Remove(part.SlotNum);
                }
            }
        }

    }
}