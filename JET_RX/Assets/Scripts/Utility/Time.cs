﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JetRx
{
    public class Time : MonoBehaviour
    {
        public static float deltaTime;

        private void Update()
        {
            deltaTime = UnityEngine.Time.deltaTime;
        }
    }
}

