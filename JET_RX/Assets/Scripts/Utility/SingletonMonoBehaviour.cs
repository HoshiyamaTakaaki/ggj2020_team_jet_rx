﻿using UnityEngine;

namespace JetRx
{
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
    {
        private static T instance;

        public static T I => instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
            }
        }
    }
}