﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JetRx
{
    public class InputManager : SingletonMonoBehaviour<InputManager>
    {
        [SerializeField] private EventSystem eventSystem;

        public void SetEnableEventSystem(bool isEnable)
        {
            eventSystem.enabled = isEnable;
        }
    }
}