﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JetRx
{
    public class SoundManager : SingletonMonoBehaviour<SoundManager>
    {
        [SerializeField] private AudioSource audioSource;

        public void PlaySE(AudioClip clip)
        {
            audioSource.PlayOneShot(clip);
        }
    }

}